# TA3_CC82_TopicosCS

 <h2 align="center">Universidad Peruana de Ciencias Aplicadas</h2>
<h2 align="center">Tópicos en Ciencias de la Computación - CC58</h2>
 
<h3 align="center"> TA3 </h3>
 
<h3 align="center"> Sección</h3>
<h3 align="center"> Profesor: Luis Martín Canaval Sanchez</h3>
<h3 align="center"> Alumnos</h3>
 <ul>
   <li align="center">Camargo Ramírez, Enzo Fabricio (U202010122)</li>
   <li align="center">Costa Mondragón, Paulo Sergio (U201912086)</li>
   <li align="center">Caballero Lara, Eduardo Roman (U202019644)</li>
 </ul>
 
 
 <h3 align="center">CICLO 2024-1</h3>

## Enunciado del problema

<p align="justify">
Constraint programming tiene muchas aplicaciones en data mining, por ejemplo en la búsqueda de conjuntos de ítems frecuentes (frequent item sets) y reglas de asociación (association rules). Por tal motivo se le pide implementar una aplicación sencilla (por ejemplo análisis de bolsa de mercado) utilizando dichas técnicas con el uso de constraint programming.

Para el desarrollo de este problema y la correcta implementación de constraint programming para la búsqueda de itemsets frecuentes al igual que reglas de asociación hemos decidido optar por un sistema de compras que demuestra los items comprados por distintos usuarios. Generando itemsets frecuentes como son los productos de compra.

Head del dataset utilizado
![image](https://github.com/PSCostaM/TA3_CC82_TopicsCs/assets/48858434/0254c27d-9653-47f1-9562-afd823709ee4)
</p>

## Tareas desarrolladas

<p align="justify">
<ul>
  ### Implementar la búsqueda de itemsets frecuentes usando constraint programming.

 La función para la búsqueda de subsets tiene como objetivo encontrar todos los subconjuntos posibles de ítems en un conjunto de transacciones y determinar cuáles de esos subconjuntos son frecuentes, es decir, aparecen al menos un número mínimo de veces (minimum support) en las transacciones.

 ```
 # Leer los datos
file_path = '/content/Online Retail.xlsx'
retail_data_sample = pd.read_excel(file_path)

# Crear la tabla de transacciones en formato adecuado
retail_data_sample['Description'] = retail_data_sample['Description'].str.strip()
basket = (retail_data_sample
          .groupby(['InvoiceNo', 'Description'])['Quantity']
          .sum().unstack().reset_index().fillna(0)
          .set_index('InvoiceNo'))

# Convertir a formato binario (0 o 1)
def encode_units(x):
    return 1 if x >= 1 else 0

basket_sets = basket.applymap(encode_units)

# Encontrar conjuntos de ítems frecuentes
min_support = 0.02  # Ajustar el soporte mínimo según sea necesario
frequent_itemsets = apriori(basket_sets, min_support=min_support, use_colnames=True)

# Obtener todas las transacciones incluyendo InvoiceNo e InvoiceDate
retail_data_sample['InvoiceDate'] = pd.to_datetime(retail_data_sample['InvoiceDate'])
invoice_dates = retail_data_sample.groupby('InvoiceNo')['InvoiceDate'].first()
transactions = [set(transaction[1].index[transaction[1] > 0]) for transaction in basket_sets.iterrows()]
invoice_numbers = [transaction[0] for transaction in basket_sets.iterrows()]
invoice_dates_list = [invoice_dates[invoice] for invoice in invoice_numbers]

# Mostrar resultados
print("Conjuntos de ítems frecuentes:", frequent_itemsets)

def clean_item_name(name):
    return re.sub(r'\W+', '_', str(name))

# Crear el modelo de programación por restricciones
model = cp_model.CpModel()

# Obtener todos los ítems únicos de los conjuntos frecuentes
all_items = set(item for itemset in frequent_itemsets['itemsets'] for item in itemset)

# Crear variables binarias para cada ítem en cada transacción
item_vars = {item: model.NewBoolVar(clean_item_name(item)) for item in all_items}

# Definir el soporte mínimo en términos de transacciones
min_support_count = 100  # Ajustar según sea necesario

# Agregar restricciones de soporte mínimo
for item in all_items:
    support = sum(1 for transaction in transactions if item in transaction)
    if support >= min_support_count:
        model.Add(item_vars[item] == 1)
    else:
        model.Add(item_vars[item] == 0)

# Resolver el modelo
solver = cp_model.CpSolver()
status = solver.Solve(model)

# Obtener los conjuntos de ítems frecuentes utilizando CP
frequent_itemsets_cp = []
if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
    for item, var in item_vars.items():
        if solver.Value(var) == 1:
            frequent_itemsets_cp.append(item)
else:
    print("No se encontraron conjuntos de ítems frecuentes con el soporte mínimo especificado.")

print("Conjuntos de ítems frecuentes (CP):", frequent_itemsets_cp)
 ```

  ### Explique su implementación de itemsets frecuentes y proporcione ejemplos:
  
  ```
  retail_data_sample['Description'] = retail_data_sample['Description'].str.strip()
basket = (retail_data_sample
          .groupby(['InvoiceNo', 'Description'])['Quantity']
          .sum().unstack().reset_index().fillna(0)
          .set_index('InvoiceNo'))
  ```

Se eliminan espacios en blanco al inicio y al final de las descripciones de los productos.

Se agrupan los datos por número de factura y descripción del producto, sumando las cantidades.

Se reorganizan los datos en una tabla donde cada fila representa una transacción y cada columna representa un ítem.

Convertimos los datos a formato binario:

```
def encode_units(x):
    return 1 if x >= 1 else 0

basket_sets = basket.applymap(encode_units)
```

Se convierte la tabla de transacciones a formato binario. Si un ítem está presente en una transacción (cantidad mayor o igual a 1), se asigna el valor 1; de lo contrario, se asigna 0.

```
min_support = 0.02  # Ajustar el soporte mínimo según sea necesario
frequent_itemsets = apriori(basket_sets, min_support=min_support, use_colnames=True)
```

Se utiliza el método apriori de mlxtend para encontrar los conjuntos de ítems frecuentes, aquellos que aparecen en al menos el min_support (2% en este caso) de las transacciones.

```
retail_data_sample['InvoiceDate'] = pd.to_datetime(retail_data_sample['InvoiceDate'])
invoice_dates = retail_data_sample.groupby('InvoiceNo')['InvoiceDate'].first()
transactions = [set(transaction[1].index[transaction[1] > 0]) for transaction in basket_sets.iterrows()]
invoice_numbers = [transaction[0] for transaction in basket_sets.iterrows()]
invoice_dates_list = [invoice_dates[invoice] for invoice in invoice_numbers]
```

Supongamos que tenemos las siguientes transacciones:

| Invoice | Description | Quantity | InvoiceDate |
| ------ | ------ | ------ | ------ | 
| 536365 | WHITE HANGING HEART T-LIGHT HOLDER | 6 | 2010-12-01 08:26:00 | 
| 536365 | WHITE METAL LANTERN | 6 | 2010-12-01 08:26:00 | 
| 536365 | CREAM CUPID HEARTS COAT HANGER | 8 | 2010-12-01 08:26:00 | 
| 536365 | KNITTED UNION FLAG HOT WATER BOTTLE | 6 | 2010-12-01 08:26:00 | 
| 536365 | RED WOOLLY HOTTIE WHITE HEART. | 6 | 2010-12-01 08:26:00 | 

Estas transacciones se convierten en una tabla binaria, donde cada columna representa un ítem y cada fila una transacción:

| InvoiceNo | WHITE HANGING HEART T-LIGHT HOLDER | WHITE METAL LANTERN | CREAM CUPID HEARTS COAT HANGER | KNITTED UNION FLAG HOT WATER BOTTLE | RED WOOLLY HOTTIE WHITE HEART. |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 536365 | 1 |1 |1 |1 |1 |

Luego, utilizando el método apriori, se encuentran los ítems frecuentes que aparecen juntos en al menos el 2% de las transacciones.

#### Crear el modelo de programación por restricciones

```
model = cp_model.CpModel()

# Obtener todos los ítems únicos de los conjuntos frecuentes
all_items = set(item for itemset in frequent_itemsets['itemsets'] for item in itemset)

# Crear variables binarias para cada ítem en cada transacción
item_vars = {item: model.NewBoolVar(clean_item_name(item)) for item in all_items}
```

#### Agregar restricciones de soporte mínimo

```
min_support_count = 100  # Ajustar según sea necesario

# Agregar restricciones de soporte mínimo
for item in all_items:
    support = sum(1 for transaction in transactions if item in transaction)
    if support >= min_support_count:
        model.Add(item_vars[item] == 1)
    else:
        model.Add(item_vars[item] == 0)
```

Se definen las restricciones de soporte mínimo. Si un ítem aparece en al menos min_support_count transacciones, se establece su variable a 1; de lo contrario, a 0.


#### Resolver el modelo y obtener los conjuntos de items frecuentes utilizando CP

```
solver = cp_model.CpSolver()
status = solver.Solve(model)

frequent_itemsets_cp = []
if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
    for item, var in item_vars.items():
        if solver.Value(var) == 1:
            frequent_itemsets_cp.append(item)
else:
    print("No se encontraron conjuntos de ítems frecuentes con el soporte mínimo especificado.")

print("Conjuntos de ítems frecuentes (CP):", frequent_itemsets_cp)
```

#### Ejemplo de salida

```
Conjuntos de ítems frecuentes (CP): ['WHITE HANGING HEART T-LIGHT HOLDER', 'WHITE METAL LANTERN', ...]
```

  ### Implementar la búsqueda de reglas de asociación usando constraint programming

```

def clean_item_name(name):
    return re.sub(r'\W+', '_', str(name))

# Crear el modelo de programación por restricciones
model = cp_model.CpModel()

# Obtener todos los ítems únicos de los conjuntos frecuentes
all_items = set(item for itemset in frequent_itemsets['itemsets'] for item in itemset)

# Crear variables binarias para cada ítem en cada transacción
item_vars = {item: model.NewBoolVar(clean_item_name(item)) for item in all_items}

# Definir el soporte mínimo en términos de transacciones
min_support_count = 100  # Ajustar según sea necesario

# Agregar restricciones de soporte mínimo
for item in all_items:
    support = sum(1 for transaction in transactions if item in transaction)
    if support >= min_support_count:
        model.Add(item_vars[item] == 1)
    else:
        model.Add(item_vars[item] == 0)

# Resolver el modelo
solver = cp_model.CpSolver()
status = solver.Solve(model)

# Obtener los conjuntos de ítems frecuentes utilizando CP
frequent_itemsets_cp = []
if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
    for item, var in item_vars.items():
        if solver.Value(var) == 1:
            frequent_itemsets_cp.append(item)
else:
    print("No se encontraron conjuntos de ítems frecuentes con el soporte mínimo especificado.")

print("Conjuntos de ítems frecuentes (CP):", frequent_itemsets_cp)

# Definir manualmente algunas reglas de asociación utilizando los ítems frecuentes obtenidos
items = frequent_itemsets_cp
confidence_threshold = 0.5
association_rules_cp = []

for i, antecedent in enumerate(items):
    for consequent in items[i+1:]:
        antecedent_support = sum(1 for transaction in transactions if antecedent in transaction)
        consequent_support = sum(1 for transaction in transactions if consequent in transaction)
        both_support = sum(1 for transaction in transactions if antecedent in transaction and consequent in transaction)

        confidence = both_support / antecedent_support if antecedent_support > 0 else 0
        lift = confidence / (consequent_support / len(transactions)) if consequent_support > 0 else 0

        if confidence >= confidence_threshold and lift > 1:  # Ajustar el umbral de lift si es necesario
            association_rules_cp.append((antecedent, consequent, confidence, lift))

print("Reglas de asociación (CP):", association_rules_cp)

# Relacionar las reglas de asociación con InvoiceNo e InvoiceDate
rules_with_invoices = []
for rule in association_rules_cp:
    antecedent, consequent, confidence, lift = rule
    related_invoices = [invoice_numbers[i] for i, transaction in enumerate(transactions) if antecedent in transaction and consequent in transaction]
    related_dates = [invoice_dates_list[i] for i, transaction in enumerate(transactions) if antecedent in transaction and consequent in transaction]
    rules_with_invoices.append((antecedent, consequent, confidence, lift, related_invoices, related_dates))

# Mostrar reglas de asociación con InvoiceNo e InvoiceDate
for rule in rules_with_invoices:
    antecedent, consequent, confidence, lift, invoices, dates = rule
    print(f"Regla: {antecedent} -> {consequent} (Confianza: {confidence}, Lift: {lift})")
    print(f"Invoices relacionados: {invoices}")
    print(f"Fechas relacionadas: {dates}\n")
```




### Explique su implementación reglas de asociación y proporcione ejemplos.

#### Crear el modelo de programación por restricciones

```
model = cp_model.CpModel()

# Obtener todos los ítems únicos de los conjuntos frecuentes
all_items = set(item for itemset in frequent_itemsets['itemsets'] for item in itemset)

# Crear variables binarias para cada ítem en cada transacción
item_vars = {item: model.NewBoolVar(clean_item_name(item)) for item in all_items}
```

Se inicializa un nuevo modelo de programación por restricciones.

Se extraen todos los ítems únicos de los conjuntos frecuentes.

Se crean variables binarias para cada ítem, donde cada variable indica la presencia (1) o ausencia (0) del ítem en una transacción.

#### Agregar restricciones de soporte mínimo

```
min_support_count = 100  # Ajustar según sea necesario

# Agregar restricciones de soporte mínimo
for item in all_items:
    support = sum(1 for transaction in transactions if item in transaction)
    if support >= min_support_count:
        model.Add(item_vars[item] == 1)
    else:
        model.Add(item_vars[item] == 0)
```

Se establece el soporte mínimo en términos del número de transacciones.

Para cada ítem, si su soporte es mayor o igual al soporte mínimo, se establece la variable correspondiente a 1; de lo contrario, a 0.

##### Resolver el modelo y obtener los ítems frecuentes utilizando CP

```
solver = cp_model.CpSolver()
status = solver.Solve(model)

frequent_itemsets_cp = []
if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
    for item, var in item_vars.items():
        if solver.Value(var) == 1:
            frequent_itemsets_cp.append(item)
else:
    print("No se encontraron conjuntos de ítems frecuentes con el soporte mínimo especificado.")

print("Conjuntos de ítems frecuentes (CP):", frequent_itemsets_cp)
```

Se resuelve el modelo CP para encontrar los ítems frecuentes.

Si el modelo es óptimo o factible, se añaden los ítems frecuentes a la lista frequent_itemsets_cp.

#### Definir manualmente algunas reglas de asociación utilizando los ítems frecuentes obtenidos

```
items = frequent_itemsets_cp
confidence_threshold = 0.5
association_rules_cp = []

for i, antecedent in enumerate(items):
    for consequent in items[i+1:]:
        antecedent_support = sum(1 for transaction in transactions if antecedent in transaction)
        consequent_support = sum(1 for transaction in transactions if consequent in transaction)
        both_support = sum(1 for transaction in transactions if antecedent in transaction and consequent in transaction)

        confidence = both_support / antecedent_support if antecedent_support > 0 else 0
        lift = confidence / (consequent_support / len(transactions)) if consequent_support > 0 else 0

        if confidence >= confidence_threshold and lift > 1:  # Ajustar el umbral de lift si es necesario
            association_rules_cp.append((antecedent, consequent, confidence, lift))

print("Reglas de asociación (CP):", association_rules_cp)
```

Se utilizan los ítems frecuentes y se establece un umbral de confianza mínimo.

Para cada par de ítems (antecedente y consecuente), se calculan el soporte conjunto, la confianza y el lift de la regla.

Se añaden a la lista association_rules_cp aquellas reglas que cumplen con los umbrales de confianza y lift.

Lift es una medida utilizada en data mining y aprendizaje por regla de asociación que ayuda para indicar el performance de un modelo al predecir o clasificar casuisticas. El modelo está haciendo un buen trabajo si la clasificación del Target (T) es mucho mejor que la baseline (B) promedio para la población. Lift es el ratio de estos valores. 
Ecuación lift:

![image](https://github.com/PSCostaM/TA3_CC82_TopicsCs/assets/48858434/20392598-dfe7-4bf3-a113-627952e8fc0d)


##### Relacionar las reglas de asociación con InvoiceNo e InvoiceDate

```
rules_with_invoices = []
for rule in association_rules_cp:
    antecedent, consequent, confidence, lift = rule
    related_invoices = [invoice_numbers[i] for i, transaction in enumerate(transactions) if antecedent in transaction and consequent in transaction]
    related_dates = [invoice_dates_list[i] for i, transaction in enumerate(transactions) if antecedent in transaction and consequent in transaction]
    rules_with_invoices.append((antecedent, consequent, confidence, lift, related_invoices, related_dates))
```

Relacionar con facturas y fechas: Para cada regla de asociación, se identifican las facturas y fechas relacionadas en las que ambos ítems (antecedente y consecuente) están presentes.

#### Mostrar reglas de asociación con InvoiceNo e InvoiceDate

```
for rule in rules_with_invoices:
    antecedent, consequent, confidence, lift, invoices, dates = rule
    print(f"Regla: {antecedent} -> {consequent} (Confianza: {confidence}, Lift: {lift})")
    print(f"Invoices relacionados: {invoices}")
    print(f"Fechas relacionadas: {dates}\n")

```

#### Ejemplo de funcionamiento

![image](https://github.com/PSCostaM/TA3_CC82_TopicsCs/assets/48858434/41f967b8-f2d7-403a-80a7-a43b7a79cf0a)


### Conclusión

<p align="justify">
 El proceso de resolución del problema implicó definir claramente los algoritmos necesarios para encontrar itemsets frecuentes y reglas de asociación, implementarlos en Python utilizando constraint programming, y crear una forma de leer datos desde un archivo xlsx. El uso de ejemplos y la explicación detallada de cada paso y algoritmo ayudan a comprender cómo funciona todo el sistema.
</p>
